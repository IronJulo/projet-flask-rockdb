from .models import Playlist, Artist, Album, Track, Score, ComposePlaylistTrack
import click
from .app import app, db
from .models import User
import json
from rich.progress import track
from hashlib import sha256
import requests

key = "523532"


@app.cli.command()
def loaddb():
    """
    Creates the tables and populates them with data.
    """
    db.drop_all()
    db.create_all()

    with open('spotify/albums_data.json') as json_file:
        data = json.load(json_file)
        for album in track(data, description="Importing Albums..."):
            album_details = data[album][0]
            db.session.add(Album(
                album_name=str(album),
                album_artist_id=str(album_details["artist_id"]),
                album_id=str(album_details["album_id"]),
                album_artist=str(album_details["artist_name"]),
                album_release_date=str(album_details["album_release"]),
                album_cover_img=str(album_details["album_cover"]),
                album_description_en=str(album_details["album_description_en"]),
                album_description_fr=str(album_details["album_description_fr"]),
                album_brainz_id=str(album_details["album_brainz_id"]),
                album_brainz_artist_id=str(album_details["album_brainz_artist_id"])
            ))
            artist_exist = Artist.query.filter_by(artist_id=album_details["artist_id"]).first()
            if not artist_exist:
                artist_img_url = fetch_artist_img_url(album_details["artist_id"], album_details["artist_name"])
                db.session.add(Artist(
                    artist_id=album_details["artist_id"],
                    artist_name=album_details["artist_name"],
                    artist_img=artist_img_url
                ))

    with open('spotify/tracks_data.json') as json_file:
        data = json.load(json_file)
        for song in track(data, description="Importing Tracks"):
            db.session.add(Track(
                track_id=song["track_id"],
                track_name=song["track_name"],
                track_id_artist=song["track_id_artist"],
                track_id_album=song["track_album_id"],
                track_artist=song["track_artist"],
                track_duration=song["track_duration"],
                track_img=song["track_img"],
                track_gender=song["track_gender"],
                track_youtube=song["track_youtube"],
                track_musicbrainz_id=song["track_musicbrainz_id"]
            ))

    m = sha256()
    l = sha256()
    m.update(str("admin").encode())
    l.update(str("toto").encode())
    u = User(user_username="admin", user_password=m.hexdigest(),
             user_img="https://eu.ui-avatars.com/api/?name={}".format("admin"),
             user_email="admin@email.com",
             is_admin_user=True)

    k = User(user_username="toto", user_password=l.hexdigest(),
             user_img="https://eu.ui-avatars.com/api/?name={}".format("toto"),
             user_email="toto@email.com",
             is_admin_user=False)

    db.session.add(u)
    db.session.add(k)

    db.session.add(Score(score_album="XOXO", score_username="admin", score_score=5))
    db.session.add(Score(score_album="Home", score_username="admin", score_score=10))
    db.session.add(Score(score_album="Rien à Branler", score_username="toto", score_score=2))
    db.session.add(Score(score_album="Nectar", score_username="toto", score_score=6))
    db.session.add(Score(score_album="The Bridge", score_username="admin", score_score=7))
    db.session.add(Score(score_album="The Bridge", score_username="toto", score_score=10))

    db.session.add(Playlist(playlist_title="Vacances",
                            playlist_cover="https://media.istockphoto.com/photos/carefree-woman-relaxation-in-swimming-pool-summer-holiday-concept-picture-id1032426322",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Romantique",
                            playlist_cover="https://i.pinimg.com/736x/f3/68/80/f36880d1fa5a1660b996e1477dd880bd.jpg",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Détente",
                            playlist_cover="https://images.pexels.com/photos/210186/pexels-photo-210186.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Concentration",
                            playlist_cover="https://www.fitnest.eu/wp-content/uploads/2019/06/stay-focused-text-in-nature-inspirational.jpg",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Chill",
                            playlist_cover="https://i.ytimg.com/vi/9r8VtP5kdoo/maxresdefault.jpg",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Joie",
                            playlist_cover="https://noemiedesaintsernin.com/wp-content/uploads/2018/02/mettre-de-la-joie-dans-sa-vie.jpg",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Le code !",
                            playlist_cover="https://korii.slate.fr/sites/default/files/styles/1440x600/public/markus-spiske-cvbbo4pzwpg-unsplash.jpg",
                            playlist_username="admin"))

    db.session.add(Playlist(playlist_title="Gaming 🎮",
                            playlist_cover="https://signal.avg.com/hubfs/Blog_Content/Avg/Signal/AVG%20Signal%20Images/9%20Ways%20to%20Boost%20Your%20Gaming%20Rig/How_to_Improve_Your_Gaming_PC_Performance-Hero.jpg",
                            playlist_username="admin"))

    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=35432434))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=35432429))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=35432424))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117248))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117252))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117236))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117249))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117250))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=33117251))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=1, compose_playlist_track_track_id=35542867))

    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=2, compose_playlist_track_track_id=35542867))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=2, compose_playlist_track_track_id=35542865))
    db.session.add(ComposePlaylistTrack(compose_playlist_track_playlist_id=2, compose_playlist_track_track_id=35542872))
    db.session.commit()


@app.cli.command()
def syncdb():
    """
    Creates all missing tables.
    """
    db.create_all()

    db.session.commit()


@app.cli.command()
@click.argument('username')
@click.argument('password')
@click.argument('email')
def new_user(username, password, email):
    """
    Add a new user.
    """

    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(user_username=username, user_password=m.hexdigest(),
             user_img="https://eu.ui-avatars.com/api/?name={}".format(username),
             user_email=email)
    db.session.add(u)
    db.session.commit()


@app.cli.command()
@click.argument('username')
@click.argument('newpassword')
def passwd(username, new_password):
    from hashlib import sha256
    m = sha256()
    m.update(new_password.encode())
    u = User.query.filter_by(user_username=username).first()
    u.password = m.hexdigest()
    db.session.commit()


def fetch_data(url):
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 '
    headers = {'User-Agent': user_agent}
    return json.loads(requests.get(url, headers=headers).text)


def fetch_artist_img_url(id_artist, artist_name):
    data = fetch_data("https://theaudiodb.com/api/v1/json/{}/artist.php?i={}".format(key, id_artist))

    invalid_url = [None, "", " ", "null"]
    if data["artists"][0]['strArtistThumb'] not in invalid_url:
        return data["artists"][0]['strArtistThumb']
    elif data["artists"][0]['strArtistWideThumb'] not in invalid_url:
        return data["artists"][0]['strArtistWideThumb']

    return "https://eu.ui-avatars.com/api/?name={}".format(artist_name)
