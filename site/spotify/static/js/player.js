var player;
var playing = false;
var player_button = document.getElementById("player_button");
var youtube_id = document.querySelector('p').dataset.youtube;


if (youtube_id !== "Unknown") {
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('video-placeholder', {
            width: 600,
            height: 400,
            videoId: youtube_id,
            playerVars: {
                color: 'white',
                'controls': 0 ,
            },
            events: {
                onReady: initialize
            }
        });
    }

    function initialize() {
        time_update_interval = setInterval(function () {
            updateTimerDisplay();
            updateProgressBar();
        }, 1000)

    }

    $('#play').on('click', function () {
        if (playing) {
            playing = false;
            player.pauseVideo();
            player_button.className = "fa fa-play";
        } else {
            playing = true;
            player.playVideo();
            player_button.className = "fa fa-pause";
        }

    });
}



